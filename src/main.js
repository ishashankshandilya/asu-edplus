import "~/assets/scss/styles.scss";
import "@rds/asuo-theme";
import DefaultLayout from "~/layouts/Default.vue";
import BootstrapVue from "bootstrap-vue";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faHome, faSignInAlt, faChevronDown, faChevronUp } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(faHome, faSignInAlt, faChevronDown, faChevronUp);

export default function(Vue, { router, head, isClient }) {
  // Set default layout as a global component
  Vue.component("Layout", DefaultLayout);
  Vue.component("font-awesome-icon", FontAwesomeIcon);
  Vue.use(BootstrapVue);

  head.meta.push({
    charset: "utf-8"
  });

  head.meta.push({
    name: "viewport",
    content: "width=device-width, initial-scale=1"
  });

  // adding CDN-hosted font
  head.link.push({
    rel: "stylesheet",
    href: "https://fonts.googleapis.com/css?family=Roboto:400,700,900&display=swap"
  });
  //adding Font-awesome icon
  head.link.push({
    rel: "stylesheet",
    href: "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
  });
}

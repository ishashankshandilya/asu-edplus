const CompressionPlugin = require("compression-webpack-plugin");

module.exports = {
  siteName: "Open courses | EdPlus",
  siteUrl: "https://rds-starter.edpl.us",
  titleTemplate: "%s",
  plugins: [],
  chainWebpack: config => {
    config.plugin("compress").use(CompressionPlugin);
    config.resolve.alias.set("@images", "@/assets/img");
  }
};
